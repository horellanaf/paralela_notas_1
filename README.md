# Tarea Paralela: Estadisticas notas

## Compilar

Para compilar se necesita g++, por lo menos la version 6.3.0

```
$ g++ main.cpp
```

## Ejecutar

Asumiendo que el archivo con las notas se encuentra en la misma carpeta que el ejecutable

```
$ ./a.out notas.txt
```

Por defecto el programa calculara la desviacion estandar muestral, si se le pasa la opcion --poblacional, entonces el programa calculara la desviacion estandar poblacional

```
$ ./a.out notas.txt --poblacional
```

## Ejemplo de ejecucion

Este es un ejemplo de la ejecución del programa, en una maquina virtual con Debian estable y un HDD de 7200 RPM. Ademas se ocupo el programa "time" para medir el tiempo de ejecucion.

```
$ time ./a.out numeros.txt --poblacional
Cantidad notas = 2147483647
Suma = 8.58981e+09
Media = 3.99994
Mediana = 4
Stdev = 1.73252
Moda = 4.4

real    18m45.611s
user    16m15.772s
sys     0m50.240s
```