// Hector Orellana
// 18.293.391-0



/* muestre por pantalla los siguientes datos: */
/* - Suma total. */
/* - Cantidad de elementos. */
/* - Media */
/* - Mediana */
/* - Moda */
/* - Desviación estándar. */

#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

class ContenedorNotas {

  // Como el tamaño del archivo que contiene las notas
  // es demasiado grande, no puedo cargar directamente las notas a la memoria.
  // Como solucion se me ocurrio tener un arreglo en el que puedo guardar todas las notas
  // (desde 1.0 a 7.0) el cual se interpreta de la siguiente manera:

  // i = indice del arreglo <=> nota
  // arreglo[i] <=> cantidad de veces que se encontro esa nota en el archivo

  // De esta manera se tiene que la nota 1.0 corresponde a la posicion 0 del arreglo,
  // la nota 1.1 a la posicion 1, etc.

  // El metodo que se encarga de hacer la transformacion es `generar_indice`
  // Con este arreglo consigo almacenar toda la informacion contenida en el archivo de 8gb
  // en un arreglo de 60 elementos.
  // Un contra de este metodo es que pierdo el orden en que las notas se encuentran en el archivo
  // pero por ahora creo que eso no importa

  // Probablemente hay una manera mas facil de hacer esta tarea,
  // pero no se me ocurrio ¯\_(ツ)_/¯.

private:
  int notas[61];

  int generar_indice(double nota) {
    // Este metodo se ve un poco complicado por la cantidad de parentesis y el uso
    // de operaciones matematicas ...

    // Creo que la siguiente tabla ayuda un poco a entender lo que ocurre aqui
    // indice  nota
    // 10      2.0
    // 11      2.1
    // 12      2.2
    // 13      2.3
    // 14      2.4
    // 15      2.5
    // 16      2.6
    // 17      2.7
    // 18      2.8
    // 19      2.9
    // 20      3.0

    // Ejemplo nota 2.0
    // El indice debe ser 10
    // a la parte entera de la nota (2) ler resto 1 y les sumo la parte decimal (.0) * 10

    // si la nota esta fuera del rango [1.0..7.0] es un error

    if (nota < 1.0 || nota > 7.0) {
      throw std::runtime_error("Nota invalida");
    }

    return (int) ((10 * ((nota - 1.0) / 1.0)) + (std::fmod(nota, 1)));
  }

  double generar_nota(int indice) {
    // La inversa de generar_indice ...

    if (indice == 0)  {
      return 1.0;
    }
    else if (indice < 10) {
      return 1.0 + ((double)indice / (double)10);
    }
    else {
      return (((double)indice / (double)10) + 1) + (std::fmod(indice, 1));
    }
  }

  int contar_notas_ingresadas() {
    // De todas las notas posibles (1.0 .. 7.0), cuenta cuales estaban en el archivo
    int contador = 0;

    for (int i = 0; i <= 60; i++) {
      if (this->notas[i] > 0)
        contador++;
    }

    return contador;
  }

  int previa(int i) {
    // encuentra cual nota esta antes de la nota en i, saltandose las notas con frecuencia 0

    for (int j = i - 1; j >= 0; j--) {
      if (this->notas[j] > 0)
        return j;
    }

    throw std::runtime_error("ContenedorNotas::previa() => Se me acabo el arreglo");
  }

public:
  ContenedorNotas() {
    for (int i = 0; i <= 60; i++) {
      this->notas[i] = 0;
    }
  }

  void mostrar_notas() {
    for (int i = 0; i <= 60; i++) {
      std::cout << i << " : " << this->generar_nota(i) << " : " << this->notas[i] << std::endl;
    }
  }

  void agregar_nota(double nota) {
    if (nota < 1.0 || nota > 7.0) {
      throw std::runtime_error("Nota invalida");
    }

    int indice = this->generar_indice(nota);
    this->notas[indice] = this->notas[indice] + 1;
  }

  double stdev_muestral() {
    double media = this->media();
    double cantidad_notas = this->contar();
    double sumatoria = 0;

    for (int i = 0; i <= 60; i++) {
      double nota = this->generar_nota(i);

      sumatoria = sumatoria + (this->notas[i] * std::pow(nota - media, 2));
    }

    return std::sqrt(sumatoria / (cantidad_notas - 1));
  }

  double stdev_poblacional() {
    double media = this->media();
    double sumatoria = 0;

    for (int i = 0; i <= 60; i++) {
      if (this->notas[i] == 0)
        continue;

      double nota = this->generar_nota(i);

      sumatoria = sumatoria + std::pow(nota, 2) * this->notas[i];
    }

    return std::sqrt((sumatoria / (double)this->contar()) - std::pow(media, 2));
  }

  double media() {
    return this->suma() / this->contar();
  }

  double suma() {
    double suma = 0.0;

    for (int i = 0; i <= 60; i++) {
      suma = suma + this->notas[i] * this->generar_nota(i);
    }

    return suma;
  }

  double mediana() {
    int cantidad = this->contar();
    int mitad = this->contar() / 2;
    bool cantidad_par = (cantidad % 2) == 0;

    int posicion = 0;

    for (int i = 0; i <= 60; i++) {
      if (this->notas[i] == 0)
        continue;

      if (posicion + this->notas[i] >= mitad) {

        if (this->notas[i] > 1 or !cantidad_par)
          return this->generar_nota(i);
        else
          return (this->generar_nota(i) + this->generar_nota(this->previa(i))) / 2;
      }

      posicion = posicion + this->notas[i];
    }

    // Nunca se deberia llegar aqui
    throw std::runtime_error("No se encontro la mediana");
  }

  std::vector<double> moda() {
    std::vector<double> modas;
    int cantidad_moda = 0;

    for (int i = 0; i<= 60; i++) {
      if (this->notas[i] == cantidad_moda) {
        double nota = this->generar_nota(i);
        modas.push_back(nota);
      }
      else if (this->notas[i] > cantidad_moda) {
        cantidad_moda = this->notas[i];
        modas.clear();

        double nota = this->generar_nota(i);
        modas.push_back(nota);
      }
    }

    if (modas.size() == this->contar_notas_ingresadas())
      // Todas las notas tienen la misma frecuencia, no hay moda
      return std::vector<double>();
    else
      return modas;
  }

  int contar() {
    int cantidad_notas = 0;

    for (int i = 0; i <= 60; i++) {
      cantidad_notas = cantidad_notas + this->notas[i];
    }

    return cantidad_notas;
  }
};

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cerr << "./a.out numeros.txt\n" << std::endl;
    return 1;
  }

  std::ifstream ifile(argv[1]);
  std::string linea;

  ContenedorNotas cn;

  while (std::getline(ifile, linea)) {
    /*
      Cambia una , por un .
      ejemplo: arreglar_coma(5,4) => 5.4
      Los numeros en el archivo numeros.txt vienen con ,
      y no los puedo pasar directamente a la fucion atof
    */
    std::replace(linea.begin(), linea.end(), ',', '.');

    double n = std::atof(linea.c_str());
    cn.agregar_nota(n);
  }

  std::cout
    << "Cantidad notas = " << cn.contar() << std::endl
    << "Suma = " << cn.suma() << std::endl
    << "Media = " << cn.media() << std::endl
    << "Mediana = " << cn.mediana() << std::endl;

  if (argc > 2) {
    std::cout << "Stdev = " << cn.stdev_poblacional() << std::endl;
  }
  else {
    std::cout << "Stdev = " << cn.stdev_muestral() << std::endl;
  }

  std::vector<double> modas = cn.moda();

  if (modas.size() > 0) {
    std::cout << "Moda = ";

    for (int i = 0; i < modas.size(); i++) {
      std::cout << modas[i];

      if (i + 1 < modas.size())
        std::cout << ", ";
    }

    std::cout << std::endl;
  }
  else {
    std::cout << "No hay moda" << std::endl;
  }

  return 0;
}
